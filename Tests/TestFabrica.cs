﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Dominio;

namespace Tests
{
    [TestFixture]
    class TestFabrica
    {

        #region Class Variables
        private Producto p1;
        private Producto p2;
        private Producto p3;
        private Fabrica f;
        private Cliente c1;
        private List<Cliente> listCliente;
        private List<Pedido> listPedidos;



        private Producto prod1;
        private Producto prod2;
        private Producto prod3;
        
        private Cliente cli1;
        private Cliente cli2;
        private Cliente cli3;
        private Cliente cli4;
        private Pedido ped1;
        private Pedido ped2;
        private Pedido ped3;
        private Pedido ped4;
        DateTime fechentrega1;
        DateTime fechentrega2;
        DateTime fechentrega3;
        DateTime fechentrega4;
        DateTime fechentrega5;
        DateTime fechentrega6;

        private List<Pedido> listPedidos1;
        private List<Pedido> listPedidos2;
        private List<Pedido> listPedidos3;
        private List<Pedido> listPedidos4;
        private Criterio crit1;
        private PlanProduccion plan;

        #endregion


        #region Setup/Teardown

       

        /// <summary>
        /// Code that is run before each test
        /// </summary>
        [SetUp]
        public void Initialize()
        {
            f = new Fabrica();
            p1 = new Producto("A", 1, 2, 1);
            p2 = new Producto("B", 2, 3, 1);
            p3 = new Producto("A", 1, 2, 1);
            c1 = new Cliente("Pedro",2);
            List<Cliente> listCliente = new List<Cliente>();
            f.Clientes = listCliente;
            listPedidos= new List<Pedido>();


            Cliente cli1 = new Cliente("Pedro", 2); 
            Cliente cli2 = new Cliente("Jei", 5);
            Cliente cli3 = new Cliente("Gonza", 8);
            Cliente cli4 = new Cliente("Pedro", 2);

            DateTime fechentrega1 = DateTime.Now.AddMinutes(50), fechentrega2 = new DateTime(2015, 5, 13), fechentrega3 = new DateTime(2015, 5, 15), fechentrega4 = DateTime.Now.AddMinutes(10);
           

            Pedido ped1 = new Pedido(5, fechentrega4, cli1, p1);//5 min de tiempo de producccion
            ped1.Estado = 'F';
            Pedido ped2 = new Pedido(3, fechentrega4, cli1, p1);//3 ""
            ped2.Estado = 'I';
            Pedido ped3 = new Pedido(1, fechentrega5, cli1, p1);//1 ""
            ped3.Estado = 'F';
            Pedido ped4 = new Pedido(6, fechentrega6, cli1, p1);//5 ""
            ped4.Estado = 'I';

            listPedidos1= new List<Pedido>();
            listPedidos2 = new List<Pedido>();
            listPedidos3 = new List<Pedido>();
            listPedidos4 = new List<Pedido>();
            
            listPedidos1.Add(ped1);
            listPedidos1.Add(ped3);
            listPedidos1.Add(ped2);
            listPedidos1.Add(ped4);

            listPedidos4.Add(ped3);
            listPedidos4.Add(ped4);
            listPedidos4.Add(ped1);
            

            crit1 = new Criterio(Criterio.tiposCriterio.Entrega, listPedidos1);
            f.Criterio = crit1;
            PlanProduccion plan = new PlanProduccion(crit1);
            
            f.Pedidos = listPedidos1;
            
            //listas para comparar resultados
            listPedidos2.Add(ped2);
            listPedidos2.Add(ped4);

            listPedidos3.Add(ped4);
            


            
       
            
            
        }

        /// <summary>
        /// Code that is run after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            p1 = p2 = p3 = null;
            c1 = null;
            listCliente = null;
            f = null;
            listPedidos = null;
            cli1 = cli2 = cli3 = cli4 = null;
            ped1 = ped2 = ped3 = ped4 = null;
            listPedidos1 = listPedidos2 = listPedidos3 = listPedidos4 = null;
            crit1 = null;
            plan = null;
            f = null;
            
        }
        #endregion






        #region TestsProducto


        

        [Test]
        public void agregarProductoAFabrica()
        {

            
            if (p1 != null)
            {
                f.AgregarProducto(p1);
            }

            Assert.True(f.ExisteProducto(p1), "No se agrego el producto a la fabrica");

        }

        [Test]
        public void modificarProducto()
        {
            
            f.AgregarProducto(p1);
            Producto aux = new Producto();
            aux = f.getProductoXNombre("A");
            f.ModificarProducto("A",p2);
            Assert.True(f.ExisteProducto(p2) && !f.ExisteProducto(aux));

        }

        [Test]
        public void getProductoPorNombre()
        {

            f.AgregarProducto(p1);
            Producto res = f.getProductoXNombre("A");
            Assert.True(res.Equals(p1));

        }

        #endregion

        #region TestsCliente

        [Test]
        public void agregarCliente() 
        {
            
          f.AgregarCliente("Pedro",2);
          Assert.True(f.ExisteCliente(c1));

        }

        [Test]
        public void testEqualsSonIguales()
        {
            Cliente cli1 = new Cliente("Pedro", 2);
            Cliente cli4 = new Cliente("Pedro", 2);
            Assert.True(cli1.Equals(cli4));

        }

        [Test]
        public void testEqualsSonDistintos()
        {
            Cliente cli1 = new Cliente("Pedro", 2);
            Cliente cli2 = new Cliente("Pablo", 5);
            Assert.False(cli1.Equals(cli2));

        }

     
       


        #endregion

        #region TestsPlanProduccion


        [Test]
        public void GenerarPlanProduccionGetPedidosPendientes()
        {
            
            f.GenerarPlan();
            CollectionAssert.AreEqual(f.PedidosPlan, listPedidos2);

        }

        [Test]
        public void GenerarPlanProduccionPedidosFueraDeFecha()
        {
            
            f.Criterio.Sort();
            f.GenerarPlan();
            List<Pedido> pedidosQueSeranEntregadosFueraDeFecha = new List<Pedido>();
            
            pedidosQueSeranEntregadosFueraDeFecha = f.GetPedidosQueSeranEntregadosFueraDeFecha();
            CollectionAssert.AreEqual(pedidosQueSeranEntregadosFueraDeFecha, listPedidos3);

        }

        [Test]
        public void GenerarPlanProduccionPedidosFueraDeFecha2()
        {
            f.Criterio.ListaPedidos = listPedidos4;
            f.Criterio.Sort();
            f.GenerarPlan();
            List<Pedido> pedidosQueSeranEntregadosFueraDeFecha = new List<Pedido>();

            pedidosQueSeranEntregadosFueraDeFecha = f.GetPedidosQueSeranEntregadosFueraDeFecha();
            CollectionAssert.AreEqual(pedidosQueSeranEntregadosFueraDeFecha, listPedidos3);

        }


       [Test]
        public void FinalizarPedidoDeUnPlanDeProduccion()
        {
           
            f.GenerarPlan();
            f.FinalizarPedido(f.PedidosPlan.ElementAt(0));

           //Si el pedido quedo como finalizado en los pedidos del plan Y también en los pedidos de la fábrica
            Assert.IsTrue((f.PedidosPlan.ElementAt(0).Estado == 'F') && (f.GetPedidoDePedidosFabrica(f.PedidosPlan.ElementAt(0)).Estado == 'F'));

        }

        [Test]
        public void testCancelarPlanDeProduccion()
        {

            f.GenerarPlan();
            f.CancelarPlan();


            Assert.IsTrue(f.esVacioElPlanDeProduccion());

        }







        #endregion



    }
}
