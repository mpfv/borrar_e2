﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Dominio;

namespace Tests
{
    [TestFixture]
    class TestCriterio
    {


        #region Class Variables
        private Producto prod1;
        private Producto prod2;
        private Producto prod3;
        private Fabrica f;

        private Cliente cli1;
        private Cliente cli2;
        private Cliente cli3;
        private Pedido ped1;
        private Pedido ped2;
        private Pedido ped3;
        private Pedido ped4;
        private Pedido ped5;
        private Pedido ped6;
        private Pedido ped7;
        DateTime fechentrega1;
        DateTime fechentrega2;
        DateTime fechentrega3;

        DateTime fechentrega4;
        DateTime fechentrega5;
        DateTime fechentrega6;
        

        private List<Pedido> listPedidos1;
        private List<Pedido> listPedidos2;
        private List<Pedido> listPedidos3;
        private Criterio crit1;
        private Criterio crit2;
        private Criterio crit3;
        private Criterio crit4;
        private List<Pedido> listPedidos4;
        private List<Pedido> listPedidos5;
        

        #endregion


        #region Setup/Teardown

 

        /// <summary>
        /// Code that is run before each test
        /// </summary>
        [SetUp]
        public void Initialize()
        {
            prod1 = new Producto("A", 1, 2, 4);//ganancia 1
            prod2 = new Producto("B", 2, 4, 1);//ganancia 2
            prod3 = new Producto("A", 1, 5, 1);//ganancia 4
            
            cli1 = new Cliente("Pedro", 2);
            cli2 = new Cliente("Jei", 5);
            cli3 = new Cliente("Gonza", 8);
            f = new Fabrica();

            DateTime fechentrega1 = new DateTime(2015,5,9);
            DateTime fechentrega2 = new DateTime(2015, 5, 13);
            DateTime fechentrega3 = new DateTime(2015, 5, 15);

            DateTime fechentrega4 = DateTime.Now.AddMinutes(10);
            DateTime fechentrega5 = DateTime.Now.AddMinutes(11);
            DateTime fechentrega6 = DateTime.Now.AddMinutes(9);


            Pedido ped1 = new Pedido(5, fechentrega1, cli3, prod1);//ganancia 5,preferencia 8
            Pedido ped2 = new Pedido(3, fechentrega2, cli2, prod1);//ganancia 3,preferencia 5
            Pedido ped3 = new Pedido(1, fechentrega3, cli1, prod1);//ganancia 1,preferencia 2
            Pedido ped4 = new Pedido(5, fechentrega3, cli1, prod1);//ganancia 5,preferencia 2

            Pedido ped5 = new Pedido(5, fechentrega4, cli3, prod1);//ganancia 5,preferencia 8
            Pedido ped6 = new Pedido(3, fechentrega5, cli2, prod1);//ganancia 3,preferencia 5
            Pedido ped7 = new Pedido(1, fechentrega6, cli1, prod1);//ganancia 1,preferencia 2

            listPedidos1 = new List<Pedido>();
            listPedidos2 = new List<Pedido>();
            listPedidos3 = new List<Pedido>();
            listPedidos4 = new List<Pedido>();
            listPedidos5 = new List<Pedido>();

            listPedidos1.Add(ped1);
            listPedidos1.Add(ped3);
            listPedidos1.Add(ped2);

            listPedidos2.Add(ped1);
            listPedidos2.Add(ped2);
            listPedidos2.Add(ped3);

            listPedidos3.Add(ped3);
            listPedidos3.Add(ped2);
            listPedidos3.Add(ped1);

            listPedidos4.Add(ped5);
            listPedidos4.Add(ped6);
            listPedidos4.Add(ped7);

            listPedidos5.Add(ped7);
            listPedidos5.Add(ped5);
            listPedidos5.Add(ped6);

            crit1 = new Criterio(Criterio.tiposCriterio.Entrega,listPedidos1);
            crit2 = new Criterio(Criterio.tiposCriterio.Entrega, listPedidos3);
            crit3 = new Criterio(Criterio.tiposCriterio.Ganancia, listPedidos1);
            crit4 = new Criterio(Criterio.tiposCriterio.Prioridad, listPedidos1);
            
            
        }

        /// <summary>
        /// Code that is run after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            prod1 = null;
            prod2 = null;
            prod3 = null;
            f = null;
            cli1 = null;
            cli2 = null;
            cli3 = null;
            ped1 = null;
            ped2 = null;
            ped3 = null;
            ped4 = null;
            listPedidos1 = null;
            listPedidos2 = null;
            listPedidos3 = null;
            listPedidos4 = null;
            listPedidos5 = null;
            crit1 = null;
            crit2 = null;
            crit3 = null;
            crit4 = null;
        }
        #endregion



        [Test]
        public void testSortEsPorFechaDeEntrega()
        {
            
            crit1.Sort();
            CollectionAssert.AreEqual(crit1.ListaPedidos, listPedidos2);
           
        }

        [Test]
        public void test2SortEsPorFechaDeEntrega()
        {
            
            crit2.Sort();
            CollectionAssert.AreEqual(crit2.ListaPedidos, listPedidos3);

        }

        [Test]
        public void test3SortEsPorFechaDeEntrega()
        {
          
            crit1.ListaPedidos = listPedidos4; 
            crit1.Sort();
            CollectionAssert.AreEqual(crit1.ListaPedidos, listPedidos5);

        }

        [Test]
        public void testSortEsPorGanancia()
        {
            
            crit3.Sort();
            CollectionAssert.AreEqual(crit3.ListaPedidos, listPedidos2);

        }

        [Test]
        public void test2SortEsPorGanancia()
        {
            crit3.ListaPedidos = listPedidos3;
            crit3.Sort();
            CollectionAssert.AreEqual(crit3.ListaPedidos, listPedidos2);

        }

        [Test]
        public void testSortEsPorPreferencia()
        {
          
            crit4.Sort();
            CollectionAssert.AreEqual(crit4.ListaPedidos, listPedidos2);

        }

        [Test]
        public void test2SortEsPorPreferencia()
        {
            crit3.ListaPedidos = listPedidos3;
            crit4.Sort();
            CollectionAssert.AreEqual(crit4.ListaPedidos, listPedidos2);

        }




    }
}
