﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Dominio;



namespace Tests
{

    [TestFixture]
    class TestProducto
    {

        #region Class Variables
        private Producto p1;
        private Producto p2;
        private Producto p3;
        

        #endregion


        #region Setup/Teardown

 

        /// <summary>
        /// Code that is run before each test
        /// </summary>
        [SetUp]
        public void Initialize()
        {
            p1 = new Producto("A", 1, 2, 1);
            p2 = new Producto("B", 2, 3, 1);
            p3 = new Producto("A", 1, 4, 1);
            
        }

        /// <summary>
        /// Code that is run after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            p1 = null;
            p2 = null;
            p3 = null;
        }
        #endregion


        #region Tests
        [Test]

        public void mismoProducto() {
            
            Assert.AreEqual(p1,p3,"Los productos son distintos");

        }

        [Test]
        public void ProductosDistintos()
        {
            Assert.AreNotEqual(p1, p2, "Los productos son distintos");

        }

        [Test]
        public void tienenIgualGanancia_CalcularGanancia()
        {
            Assert.IsTrue(p1.calcularGanancia() == p2.calcularGanancia());

        }

        [Test]
        public void tienenDiferenteGanancia_CalcularGanancia()
        {
            Assert.IsTrue(p1.calcularGanancia() != p3.calcularGanancia());

        }

     



        #endregion

    }
}
