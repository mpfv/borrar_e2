﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Dominio;

namespace Tests
{
    [TestFixture]
    class TestPedido
    {

        #region Class Variables
        private Producto p1;
        private Producto p2;
        private Producto p3;
        private Fabrica f;
        private Cliente c1;
        private List<Cliente> listCliente;
        private List<Pedido> listPedidos;



        private Producto prod1;
        private Producto prod2;
        private Producto prod3;

        private Cliente cli1;
        private Cliente cli2;
        private Cliente cli3;
        private Cliente cli4;
        private Pedido ped1;
        private Pedido ped2;
        private Pedido ped3;
        private Pedido ped4;
        DateTime fechentrega1;
        DateTime fechentrega2;
        DateTime fechentrega3;
        DateTime fechentrega4;
        DateTime fechentrega5;
        DateTime fechentrega6;

        private List<Pedido> listPedidos1;
        private List<Pedido> listPedidos2;
        private List<Pedido> listPedidos3;
        private List<Pedido> listPedidos4;
        private Criterio crit1;
        private PlanProduccion plan;

        #endregion


        #region Setup/Teardown



        /// <summary>
        /// Code that is run before each test
        /// </summary>
        [SetUp]
        public void Initialize()
        {
            f = new Fabrica();
            p1 = new Producto("A", 1, 2, 3);
            p2 = new Producto("B", 2, 2, 3);
            p3 = new Producto("A", 1, 2, 3);
            c1 = new Cliente("Pedro", 2);
            List<Cliente> listCliente = new List<Cliente>();
            f.Clientes = listCliente;
            listPedidos = new List<Pedido>();


            Cliente cli1 = new Cliente("Pedro", 2);
            Cliente cli2 = new Cliente("Jei", 5);
            Cliente cli3 = new Cliente("Gonza", 8);
            Cliente cli4 = new Cliente("Pedro", 2);

            DateTime fechentrega1 = DateTime.Now.AddMinutes(50), fechentrega2 = new DateTime(2015, 5, 13), fechentrega3 = new DateTime(2015, 5, 15), fechentrega4 = DateTime.Now.AddMinutes(10);


            Pedido ped1 = new Pedido(5, fechentrega4, cli1, p1);//5 min de tiempo de producccion
            ped1.Estado = 'F';
            Pedido ped2 = new Pedido(3, fechentrega4, cli1, p1);//3 ""
            ped2.Estado = 'I';
            Pedido ped3 = new Pedido(1, fechentrega5, cli1, p1);//1 ""
            ped3.Estado = 'F';
            Pedido ped4 = new Pedido(6, fechentrega6, cli1, p1);//5 ""
            ped4.Estado = 'I';

            listPedidos1 = new List<Pedido>();
            listPedidos2 = new List<Pedido>();
            listPedidos3 = new List<Pedido>();
            listPedidos4 = new List<Pedido>();

            listPedidos1.Add(ped1);
            listPedidos1.Add(ped3);
            listPedidos1.Add(ped2);
            listPedidos1.Add(ped4);

            listPedidos4.Add(ped3);
            listPedidos4.Add(ped4);
            listPedidos4.Add(ped1);


            crit1 = new Criterio(Criterio.tiposCriterio.Entrega, listPedidos1);
            f.Criterio = crit1;
            PlanProduccion plan = new PlanProduccion(crit1);

            f.Pedidos = listPedidos1;

            //listas para comparar resultados
            listPedidos2.Add(ped2);
            listPedidos2.Add(ped4);

            listPedidos3.Add(ped4);







        }

        /// <summary>
        /// Code that is run after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            p1 = p2 = p3 = null;
            c1 = null;
            listCliente = null;
            f = null;
            listPedidos = null;
            cli1 = cli2 = cli3 = cli4 = null;
            ped1 = ped2 = ped3 = ped4 = null;
            listPedidos1 = listPedidos2 = listPedidos3 = listPedidos4 = null;
            crit1 = null;
            plan = null;
            f = null;

        }
        #endregion


        [Test]
        public void CalcularGanancia1()
        {
            Assert.IsTrue(ped1.calcularGanancia() == 5);

        }

        [Test]
        public void CalcularGanancia2()
        {
            Assert.IsTrue(ped2.calcularGanancia() == 3);

        }

        [Test]
        public void CalcularGanancia3()
        {
            Assert.IsTrue(ped4.calcularGanancia() == 6);

        }


    }
}
