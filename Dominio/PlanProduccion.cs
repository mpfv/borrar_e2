﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
   public class PlanProduccion
    {


        private Criterio criterio;
        private List<Pedido> pedidosPlan;

        public Criterio Criterio
        {
            get { return criterio; }
            set { criterio = value; }
        }
        

        public List<Pedido> PedidosPlan
        {
            get { return pedidosPlan; }
            set { pedidosPlan = value; }
        }

        public PlanProduccion(Criterio pcriterio)
        {
            criterio = pcriterio;
            pedidosPlan = new List<Pedido>();
        }




    }
}
