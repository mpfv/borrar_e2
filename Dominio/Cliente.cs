﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Cliente
    {
        
        private string nombre;
        private decimal preferencia;


  
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        

        public decimal Preferencia
        {
            get { return preferencia; }
            set { preferencia = value; }
        }

        public Cliente() { }

        public Cliente(string pnombre, decimal ppreferencia)
        {
            
            nombre = pnombre;
            preferencia = ppreferencia;
        }



        public override string ToString()
        {
            
            return nombre +", " +preferencia;
        }


        public override bool Equals(Object obj)
        {

            if (obj == null)
            {
                return false;
            }

            Cliente c = obj as Cliente;
            if ((System.Object)c == null)
            {
                return false;
            }

            return nombre == c.Nombre;
        }

        public bool Equals(Cliente cli)
        {
            if (cli == null) return false;
            return (this.nombre.Equals(cli.nombre));
        }


        public decimal getPreferencia() 
        {
            return Preferencia;
        }
    }
}
