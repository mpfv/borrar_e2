﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Fabrica
    {
        private static Fabrica instancia;
        private List<Producto> productos;
        private List<Cliente> clientes;
        private List<Pedido> pedidos;
        private List<Pedido> pedidosPlan;
        private PlanProduccion planProduccion;
        private Criterio criterio;

        public Criterio Criterio
        {
            get { return criterio; }
            set { criterio = value; }
        }

        public PlanProduccion PlanProduccion
        {
            get { return planProduccion; }
            set { planProduccion = value; }
        }


        public List<Pedido> Pedidos
        {
            get { return pedidos; }
            set { pedidos = value; }
        }
        

        public List<Pedido> PedidosPlan
        {
            get { return pedidosPlan; }
            set { pedidosPlan = value; }
        }


        public List<Cliente> Clientes
        {
            get { return clientes; }
            set { clientes = value; }
        }

        public List<Producto> Productos
        {
            get { return productos; }
            set { productos = value; }
        }

        public Fabrica()
        {
            //criterio = Criterio.Prioridad;
            clientes = new List<Cliente>();
            productos = new List<Producto>();
            pedidos = new List<Pedido>();
            pedidosPlan = new List<Pedido>();
            planProduccion = new PlanProduccion(criterio);
       
        }

        public static Fabrica Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new Fabrica();
                }
                return instancia;
            }
        }

      

    #region Producto    

        public void AgregarProducto(String pnombre, String pcosto, String pprecio, String ptiempo)
        {
            Producto p = new Producto(pnombre, pcosto, pprecio, ptiempo);

            if (!ExisteProducto(p))
            {
                productos.Add(p);
            }
            else
            {
                throw new Dominio.Excepciones.AlreadyExistException();
            }

        }

        public void AgregarProducto(Producto p)
        {
           
            if (!ExisteProducto(p))
            {
                productos.Add(p);
            }
            else
            {
                throw new Dominio.Excepciones.AlreadyExistException();
            }

        }


        public void EliminarProducto(Producto p)
        {
            if (ExisteProducto(p))
            {
                productos.Remove(p);
            }
            else
            {
                throw new Dominio.Excepciones.NotExistException();
            }
        }

        public bool ExisteProducto(Producto pproducto)
        {
            return productos.Contains(pproducto);
        }

        public List<Producto> getProductos()
        {
            return productos;
        }

        public Producto getProductoXNombre(String nombre) 
        {
            Producto result = null;
            
            foreach (Producto p in this.Productos) 
            {
                if (p.Nombre.Equals(nombre))
                    result = p;
            }
            return result;
        }

        public void ModificarProducto(String nom,Producto pnuevo) 
        {
            EliminarProducto(getProductoXNombre(nom));
            AgregarProducto(pnuevo);

        
        }

    #endregion

    #region Cliente
        public void AgregarCliente(String pnombre, decimal ppreferencia)
        {
            Cliente c = new Cliente(pnombre, ppreferencia);

            if (!ExisteCliente(c))
            {
                clientes.Add(c);
            }
            else
            {
                throw new Dominio.Excepciones.AlreadyExistException();
            }

        }

        public void EliminarCliente(Cliente c)
        {
            if (ExisteCliente(c))
            {
                clientes.Remove(c);
            }
            else
            {
                throw new Dominio.Excepciones.NotExistException();
            }
        }

        public bool ExisteCliente(Cliente pcliente)
        {
            return clientes.Contains(pcliente);
        }

        public List<Cliente> getClientes()
        {
            return clientes;
        }
            
    #endregion

    #region Pedidos

        public Pedido GetPedidoDePedidosFabrica(Pedido unpedido)
        {
            Pedido retorno = new Pedido();
            foreach (Pedido p in Pedidos) {
                if (p.Equals(unpedido))
                    retorno = p;
            }
            return retorno;
        }

        public void RegistrarPedido(decimal pcantidad, DateTime pfechaEntrega, Cliente pcliente, Producto pproducto)
        {
            Pedido p = new Pedido(pcantidad, pfechaEntrega, pcliente, pproducto);
            pedidos.Add(p);
        }

        public void FinalizarPedido(Pedido p)
        {
            if (ExistePedido(p))
            {
                
                p.FinalizarPedido();
                FinalizarPedidoEnPedidosFabrica(p);
            }
        }

        public void FinalizarPedidoEnPedidosFabrica(Pedido pedido)
        {
            foreach (Pedido p in pedidos) { 
                if (p.Equals(pedido))
                    p.FinalizarPedido();
            }
        }

        private bool ExistePedido(Pedido ppedido)
        {
            return pedidosPlan.Contains(ppedido);
        }
    #endregion

    #region PlanProduccion



        public List<Pedido> GenerarPlan()
        {
            pedidosPlan = GetPedidosPendientes();
            Criterio.Sort();
            return GetPedidosQueSeranEntregadosFueraDeFecha();
        }

        public List<Pedido> GetPedidosQueSeranEntregadosFueraDeFecha() 
        {
            List<Pedido> retorno = new List<Pedido>();
            int tiempoTotal = 0;
            foreach(Pedido ped in PedidosPlan){
                tiempoTotal += Convert.ToInt32(ped.Cantidad) * ped.Producto.TiempoFabricacionUnidad;
                if (ped.FechaEntrega < (ped.Inicio.AddMinutes(tiempoTotal)))
                    retorno.Add(ped);
                

            }
            return retorno;
        }

        public void CancelarPlan()
        {
            
            pedidosPlan = null;
        }

        public bool esVacioElPlanDeProduccion()
        {
            return pedidosPlan == null;
        }

        public List<Pedido> GetPedidosPendientes()
        {
            List<Pedido> retorno = new List<Pedido>();

            foreach (Pedido p in pedidos)
            {
                if (!p.Finalizado())
                {
                    retorno.Add(p);
                }
            }

            return retorno;
        }

        public List<Pedido> GetPedidosPlan()
        {
            return pedidosPlan;
        }

        public void EliminarPedidoPlan(Pedido p)
        {
            if (ExistePedido(p))
            {
                pedidosPlan.Remove(p);
            }
            else
            {
                throw new Dominio.Excepciones.NotExistException();
            }
        }

        
    #endregion




      
    }
}
