﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Criterio
    {
        public enum tiposCriterio
        {
            Entrega,
            Ganancia,
            Prioridad
        }
        private tiposCriterio tipo;

        public tiposCriterio Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        private List<Pedido> listaPedidos;

        public List<Pedido> ListaPedidos
        {
            get { return listaPedidos; }
            set { listaPedidos = value; }
        }


        public Criterio()
        {


        }


        public Criterio(tiposCriterio ptipo, List<Pedido> plistPedidos)
        {
            
            tipo = ptipo;
            listaPedidos = plistPedidos;

        }

        public void porFechaDeEntrega()
        {
            
            listaPedidos.Sort((x, y) => -y.FechaEntrega.CompareTo(x.FechaEntrega));

        }

        public void porGanancia()
        {
            listaPedidos.Sort((x, y) => y.calcularGanancia().CompareTo(x.calcularGanancia()));

        }

        public void porPreferencia()
        {
            listaPedidos.Sort((x, y) => y.getPreferenciaCliente().CompareTo(x.getPreferenciaCliente()));

        }

        public void Sort()
        {
            if (tipo == tiposCriterio.Entrega) porFechaDeEntrega();
            if (tipo == tiposCriterio.Ganancia) porGanancia();
            if (tipo == tiposCriterio.Prioridad) porPreferencia();
        }

        public override string ToString()
        {

            return tipo.ToString();
        } 


    }
}
