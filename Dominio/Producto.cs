﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Producto
    {

        private Guid id;
        private String nombre;
        private double costoProduccionUnidad;
        private double precioVentaUnidad;
        private int tiempoFabricacionUnidad;

      


        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }

        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public double CostoProduccionUnidad
        {
            get { return costoProduccionUnidad; }
            set { costoProduccionUnidad = value; }

        }

        public double PrecioVentaUnidad
        {
            get { return precioVentaUnidad; }
            set { precioVentaUnidad = value; }
        }

        public int TiempoFabricacionUnidad
        {
            get { return tiempoFabricacionUnidad; }
            set { tiempoFabricacionUnidad = value; }
        }
        
        public Producto() { }

        public Producto(String pnombre,double pcosto, double pprecio, int ptiempo)
        {
            id = Guid.NewGuid();
            nombre = pnombre;
            costoProduccionUnidad = pcosto;
            precioVentaUnidad = pprecio;
            tiempoFabricacionUnidad = ptiempo;
        }

        internal Producto(String pnombre,String pcosto, String pprecio, String ptiempo)
        {
            nombre = pnombre;
            costoProduccionUnidad = Convert.ToDouble(pcosto);
            precioVentaUnidad = Convert.ToDouble(pprecio);
            tiempoFabricacionUnidad = Convert.ToInt16(ptiempo);
        }

        public override bool Equals(Object obj)
        {

            if (obj == null)
            {
                return false;
            }

            Producto p = obj as Producto;
            if ((System.Object)p == null)
            {
                return false;
            }

            return nombre == p.Nombre ;
        }

        public override string ToString()
        {
            String s = "Nombre:" + nombre +  "  Costo:" + costoProduccionUnidad + "  Precio:" + precioVentaUnidad + "  Tiempo: " + tiempoFabricacionUnidad;
            return s;
        }


        public double calcularGanancia()
        {
            return precioVentaUnidad - costoProduccionUnidad;
        }

    }
}



 