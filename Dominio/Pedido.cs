﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Pedido
    {
        private decimal cantidad;
        private DateTime fechaEntrega;
        private char estado;
        private DateTime inicio;
        private DateTime fin;
        private Producto producto;
        private Cliente cliente;


        public decimal Cantidad
        {
            get { return cantidad; }
            set { cantidad = value; }
        }
        

        public DateTime FechaEntrega
        {
            get { return fechaEntrega; }
            set { fechaEntrega = value; }
        }
        

        public char Estado
        {
            get { return estado; }
            set { estado = value; }
        }
        

        public DateTime Inicio
        {
            get { return inicio; }
            set { inicio = value; }
        }
        

        public DateTime Fin
        {
            get { return fin; }
            set { fin = value; }
        }
        

        public Producto Producto
        {
            get { return producto; }
            set { producto = value; }
        }
        

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }






        public Pedido() { }

        public Pedido(decimal pcantidad, DateTime pfechaEntrega, char pestado)
        {
            
            cantidad = pcantidad;
            fechaEntrega = pfechaEntrega;
            estado = pestado;
            inicio = DateTime.Now;
            fin = DateTime.Now;
        }

        public Pedido(decimal pcantidad, DateTime pfechaEntrega)
        {
            cantidad = pcantidad;
            fechaEntrega = pfechaEntrega;
            estado = 'I';
            inicio = DateTime.Now;
            fin = DateTime.Now;
            
        }

        public Pedido(decimal pcantidad, DateTime pfechaEntrega, Cliente pcliente, Producto pproducto)
        {
            cantidad = pcantidad;
            fechaEntrega = pfechaEntrega;
            estado = 'I';
            inicio = DateTime.Now;
            fin = DateTime.Now;
            cliente = pcliente;
            producto = pproducto;
        }

        public override string ToString()
        {
           // String s = "Cliente: " + cliente.ToString() + "  Producto:" + producto.Nombre + "  Cantidad:" + cantidad + "  Fecha Entrega:" + fechaEntrega;
            //return s;
            return Convert.ToString(producto) + " * " + Convert.ToString(cantidad);
        }

        public override bool Equals(Object obj)
        {

            if (obj == null)
            {
                return false;
            }

            Pedido ped = obj as Pedido;
            if ((System.Object)ped == null)
            {
                return false;
            }

            bool retorno = Cliente.Equals(ped.Cliente) && Producto.Equals(ped.Producto) && Cantidad == ped.Cantidad && FechaEntrega == ped.FechaEntrega;
            return retorno;
        }

        public bool Finalizado()
        {
            bool ret = false;
            if (estado == 'F')
            {
                ret = true;
            }
            return ret;

        }

        public void FinalizarPedido()
        {
            estado = 'F';
            inicio = DateTime.Now;
            fin = DateTime.Now;
        }

        public double calcularGanancia()
        {
            return Producto.calcularGanancia() * Convert.ToInt32(cantidad);
        }





        public decimal getPreferenciaCliente()
        {
            return Cliente.getPreferencia();
        }


    }
}