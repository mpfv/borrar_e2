﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dominio;

namespace Interfaz
{
    public partial class ListaProductos : Form
    {
        private Fabrica sistema = Fabrica.Instancia;

        public ListaProductos()
        {
            InitializeComponent();
            this.dgProductos.DataSource = null;
            this.dgProductos.DataSource = sistema.Productos;
            btnEditar.Enabled = false;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ABMProductos newProd = new ABMProductos(null);
            newProd.ShowDialog();
            this.dgProductos_UpdateDataSource();
        }

        private void dgProductos_UpdateDataSource()
        {
            this.dgProductos.DataSource = null;
            this.dgProductos.DataSource = this.sistema.Productos;
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            this.dgProductos_UpdateDataSource();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Antes de eliminar verifique que el producto no está asociado a pedidos.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
