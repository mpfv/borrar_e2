﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dominio;

namespace Interfaz
{
    public partial class ABMProductos : Form
    {
        private Fabrica sistema = Fabrica.Instancia;
        private Producto producto = null;

        public ABMProductos(Producto p)
        {
            InitializeComponent();
            this.producto = p;

            if (p != null)
            {
                this.txt_Costo.Text = p.CostoProduccionUnidad.ToString();
                this.txt_Precio.Text = p.PrecioVentaUnidad.ToString();
                this.txt_Tiempo.Text = p.TiempoFabricacionUnidad.ToString();
                
            }
        }

        private bool ValidateForm()
        {
            bool retorno = true;

            retorno = !Validaciones.Validaciones.IsEmptyField(this.txt_Nombre);
            if (!retorno)
            {
                MessageBox.Show("Debe ingresar el nombre del Producto.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            retorno = !Validaciones.Validaciones.IsEmptyField(this.txt_Costo);
            if (!retorno)
            {
                MessageBox.Show("Debe ingresar el costo del Producto.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            retorno = !Validaciones.Validaciones.IsEmptyField(this.txt_Precio);
            if (!retorno)
            {
                MessageBox.Show("Debe ingresar precio del Producto.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            retorno = !Validaciones.Validaciones.IsEmptyField(this.txt_Tiempo);
            if (!retorno)
            {
                MessageBox.Show("Debe ingresar tiempo del Producto.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;
        }

        private void btn_Guardar_Click(object sender, EventArgs e)
        {
            if (this.ValidateForm())
            {
                if (this.producto == null)
                {
                    try
                    {
                        this.sistema.AgregarProducto(txt_Nombre.Text,txt_Costo.Text, txt_Precio.Text, txt_Tiempo.Text);
                        MessageBox.Show("El producto se guardo correctamente en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    catch (Dominio.Excepciones.AlreadyExistException)
                    {
                        MessageBox.Show("Ya existe este producto en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Error inesperado al intentar guardar el producto, verifique los datos ingresados.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
