﻿namespace Interfaz
{
    partial class SetCriterio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmb_Criterios = new System.Windows.Forms.ComboBox();
            this.lbl_CritDisponibles = new System.Windows.Forms.Label();
            this.Llb_Criterio = new System.Windows.Forms.Label();
            this.btn_Cambiar = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmb_Criterios
            // 
            this.cmb_Criterios.FormattingEnabled = true;
            this.cmb_Criterios.Location = new System.Drawing.Point(48, 76);
            this.cmb_Criterios.Name = "cmb_Criterios";
            this.cmb_Criterios.Size = new System.Drawing.Size(212, 21);
            this.cmb_Criterios.TabIndex = 0;
            this.cmb_Criterios.SelectedIndexChanged += new System.EventHandler(this.cmb_Criterios_SelectedIndexChanged);
            // 
            // lbl_CritDisponibles
            // 
            this.lbl_CritDisponibles.AutoSize = true;
            this.lbl_CritDisponibles.Location = new System.Drawing.Point(12, 51);
            this.lbl_CritDisponibles.Name = "lbl_CritDisponibles";
            this.lbl_CritDisponibles.Size = new System.Drawing.Size(158, 13);
            this.lbl_CritDisponibles.TabIndex = 1;
            this.lbl_CritDisponibles.Text = "Criterios del plan de producción:";
            // 
            // Llb_Criterio
            // 
            this.Llb_Criterio.AutoSize = true;
            this.Llb_Criterio.Location = new System.Drawing.Point(12, 18);
            this.Llb_Criterio.Name = "Llb_Criterio";
            this.Llb_Criterio.Size = new System.Drawing.Size(121, 13);
            this.Llb_Criterio.TabIndex = 2;
            this.Llb_Criterio.Text = "CRITERIO SETEADO:  ";
            // 
            // btn_Cambiar
            // 
            this.btn_Cambiar.Location = new System.Drawing.Point(80, 122);
            this.btn_Cambiar.Name = "btn_Cambiar";
            this.btn_Cambiar.Size = new System.Drawing.Size(75, 23);
            this.btn_Cambiar.TabIndex = 3;
            this.btn_Cambiar.Text = "Cambiar";
            this.btn_Cambiar.UseVisualStyleBackColor = true;
            this.btn_Cambiar.Click += new System.EventHandler(this.btn_Cambiar_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(161, 122);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_Cancel.TabIndex = 4;
            this.btn_Cancel.Text = "Cancelar";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // SetCriterio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 157);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Cambiar);
            this.Controls.Add(this.Llb_Criterio);
            this.Controls.Add(this.lbl_CritDisponibles);
            this.Controls.Add(this.cmb_Criterios);
            this.Name = "SetCriterio";
            this.Text = "Criterio";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmb_Criterios;
        private System.Windows.Forms.Label lbl_CritDisponibles;
        private System.Windows.Forms.Label Llb_Criterio;
        private System.Windows.Forms.Button btn_Cambiar;
        private System.Windows.Forms.Button btn_Cancel;
    }
}