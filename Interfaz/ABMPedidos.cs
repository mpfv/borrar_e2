﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dominio;

namespace Interfaz
{
    public partial class ABMPedidos : Form
    {

        private Fabrica sistema = Fabrica.Instancia;
        private Pedido pedido = null;
        private Cliente cliente = null;
        private Producto producto = null;

        public ABMPedidos()
        {
            InitializeComponent();
            dgClientes_dgProductos_UpdateDataSource();      
        }

        private void dgClientes_dgProductos_UpdateDataSource()
        {
            this.dgClientes.DataSource = null;
            this.dgProductos.DataSource = null;
            this.dgClientes.DataSource = sistema.Clientes;
            this.dgProductos.DataSource = sistema.Productos;
        }

        private bool ValidateForm()
        {
            bool retorno = true;

            retorno = !Validaciones.Validaciones.IsEmptyField(this.Nrc_Cantidad);
            if (!retorno)
            {
                MessageBox.Show("Debe ingresar la cantidad del pedido.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            retorno = !Validaciones.Validaciones.IsEmptyField(this.dt_Entrega);
            if (!retorno)
            {
                MessageBox.Show("Debe ingresar fecha de entrega del producto.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            retorno = !Validaciones.Validaciones.IsDecimalField(this.Nrc_Cantidad);
            if (retorno)
            {
                MessageBox.Show("La informacion ingresada de cantidad no es correcta, verifique y vuelva a intentar.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            retorno = Validaciones.Validaciones.IsPedidoCantidadOk(this.Nrc_Cantidad);
            if (!retorno)
            {
                MessageBox.Show("Verifique valor de cantidad del pedido.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                retorno = Validaciones.Validaciones.IsPedidoFechaOk(this.dt_Entrega);
                if (!retorno)
                {
                    MessageBox.Show("Verifique fecha del pedido.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            return retorno;
        } 

        private void btn_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void btn_Registrar_Click(object sender, EventArgs e)
        {

            if (this.dgClientes.SelectedRows.Count > 0 && this.dgProductos.SelectedRows.Count > 0)
            {
                Cliente cli = null;
                Producto prod = null;
                DateTime fch = DateTime.Now;
                decimal cant = 0;

                foreach (DataGridViewRow row in this.dgClientes.SelectedRows)
                {
                    try
                    {
                        cli = row.DataBoundItem as Cliente;
                    }
                    catch (Dominio.Excepciones.NotExistException)
                    {
                        MessageBox.Show("El cliente seleccionado no se encontró en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                foreach (DataGridViewRow row in this.dgProductos.SelectedRows)
                {
                    try
                    {
                        prod = row.DataBoundItem as Producto;
                    }
                    catch (Dominio.Excepciones.NotExistException)
                    {
                        MessageBox.Show("El producto seleccionado no se encontró en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                try
                {
                    cant = decimal.Parse(this.Nrc_Cantidad.Value.ToString());
                    fch = this.dt_Entrega.Value;

                }
                catch (Exception)
                {
                    MessageBox.Show("Algunos de los datos ingresados no son correctos, verifique y vuelva a intentarlo.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                if (cli != null && prod != null)
                {
                    if (this.ValidateForm())
                    {
                        try
                        {
                            this.sistema.RegistrarPedido(cant, fch, cli, prod);
                            MessageBox.Show("El pedido se ha registrado correctamente en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                        catch (Dominio.Excepciones.NotExistException)
                        {
                            MessageBox.Show("El cliente o producto seleccionados no se encontraron en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Dominio.Excepciones.AlreadyExistException)
                        {
                            MessageBox.Show("Se encontro un error inesperado, por algun motivo este pedido ya existe en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un cliente y un producto para registrar un pedido.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }



    }
}
