﻿namespace Interfaz
{
    partial class AcercaDe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Auts = new System.Windows.Forms.Label();
            this.lbl_Tit = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_Auts
            // 
            this.lbl_Auts.AutoSize = true;
            this.lbl_Auts.Location = new System.Drawing.Point(43, 49);
            this.lbl_Auts.Name = "lbl_Auts";
            this.lbl_Auts.Size = new System.Drawing.Size(198, 13);
            this.lbl_Auts.TabIndex = 0;
            this.lbl_Auts.Text = "Autores: Mariela Frejlich, Álvaro Ceballos";
            // 
            // lbl_Tit
            // 
            this.lbl_Tit.AutoSize = true;
            this.lbl_Tit.Location = new System.Drawing.Point(43, 24);
            this.lbl_Tit.Name = "lbl_Tit";
            this.lbl_Tit.Size = new System.Drawing.Size(180, 13);
            this.lbl_Tit.TabIndex = 1;
            this.lbl_Tit.Text = "Diseño de Aplicaciones - Mayo 2015";
            // 
            // AcercaDe
            // 
            this.ClientSize = new System.Drawing.Size(284, 98);
            this.Controls.Add(this.lbl_Tit);
            this.Controls.Add(this.lbl_Auts);
            this.Name = "AcercaDe";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        //private System.Windows.Forms.Label lbl_Autores;
        //private System.Windows.Forms.Label lbl_;
        private System.Windows.Forms.Label lbl_Auts;
        private System.Windows.Forms.Label lbl_Tit;
    }
}