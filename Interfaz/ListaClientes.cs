﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dominio;

namespace Interfaz
{
    public partial class ListaClientes : Form
    {
        private Fabrica sistema = Fabrica.Instancia;

        public ListaClientes()
        {
            InitializeComponent();
            this.dgClientes.DataSource = null;
            this.dgClientes.DataSource = sistema.Clientes;
            btnEditar.Enabled = false;
        }

        private void dgClientes_UpdateDataSource()
        {
            this.dgClientes.DataSource = null;
            this.dgClientes.DataSource = this.sistema.Clientes;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ABMClientes newCli = new ABMClientes(null);
            newCli.ShowDialog();
            this.dgClientes_UpdateDataSource();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (this.dgClientes.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow row in this.dgClientes.SelectedRows)
                {
                    try
                    {
                        Cliente c = row.DataBoundItem as Cliente;
                        if (c != null)
                        {
                            // this.sistema.EliminarCliente(c);
                            MessageBox.Show("Antes de eliminar verifique que el cliente no está asociado a pedidos.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Dominio.Excepciones.NotExistException)
                    {
                        MessageBox.Show("El cliente seleccionado no se encontró en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                this.dgClientes_UpdateDataSource();
            }
            else
            {
                MessageBox.Show("Debe seleccionar el cliente que quiere eliminar.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (this.dgClientes.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow row in this.dgClientes.SelectedRows)
                {
                    try
                    {
                        Cliente c = row.DataBoundItem as Cliente;
                        if (c != null)
                        {
                            ABMClientes newCli = new ABMClientes(c);
                            newCli.Show();
                        }
                    }
                    catch (Dominio.Excepciones.NotExistException)
                    {
                        MessageBox.Show("El cliente seleccionado no se encontró en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                this.dgClientes_UpdateDataSource();
            }
            else
            {
                MessageBox.Show("Debe seleccionar el cliente que quiere editar.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            this.dgClientes_UpdateDataSource();
        }

        private void dgClientes_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Exception != null && e.Context == DataGridViewDataErrorContexts.Commit)
            {
                MessageBox.Show("Error inesperado al eliminar el cliente.");
            }
        }
    }
}
