﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dominio;

namespace Interfaz
{
    public partial class ListaPedidos : Form
    {
        private Fabrica sistema = Fabrica.Instancia;

        public ListaPedidos()
        {
            InitializeComponent();
            this.dgPedidos.DataSource = null;
            this.dgPedidos.DataSource = sistema.Pedidos;
        }

        private void dgPedidos_UpdateDataSource()
        {
            this.dgPedidos.DataSource = null;
            this.dgPedidos.DataSource = this.sistema.Pedidos;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ABMPedidos newPed = new ABMPedidos();
            newPed.ShowDialog();
            this.dgPedidos_UpdateDataSource();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            this.dgPedidos_UpdateDataSource();
        }


    }
}
