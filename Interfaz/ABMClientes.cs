﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dominio;

namespace Interfaz
{
    public partial class ABMClientes : Form
    {
        private Fabrica sistema = Fabrica.Instancia;
        private Cliente cliente = null;

        public ABMClientes(Cliente c)
        {
            this.cliente = c;
            InitializeComponent();

            if (c != null)
            {
                this.txt_CliNom.Text = c.Nombre;
                this.Nrc_CliPref.Value = c.Preferencia;
            }

        }

        private void ABMClientes_Load(object sender, EventArgs e)
        {

        }

        private bool ValidateForm()
        {
            bool retorno = true;

            retorno = !Validaciones.Validaciones.IsEmptyField(this.txt_CliNom);
            if (!retorno)
            {
                MessageBox.Show("Debe ingresar el nombre del Cliente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            retorno = !Validaciones.Validaciones.IsEmptyField(this.Nrc_CliPref);
            if (!retorno)
            {
                MessageBox.Show("Debe ingresar preferencia del Cliente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            retorno = Validaciones.Validaciones.IsClientePreferenciaOk(this.Nrc_CliPref);
            if (!retorno)
            {
                MessageBox.Show("Verifique valor de preferencia del Cliente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            

            return retorno;
        }

        private void btn_Guardar_Click(object sender, EventArgs e)
        {
            if (this.ValidateForm())
            {
                if (this.cliente == null)
                {
                    try
                    {
                        this.sistema.AgregarCliente(txt_CliNom.Text, Nrc_CliPref.Value);
                        MessageBox.Show("El cliente se guardo correctamente en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    catch (Dominio.Excepciones.AlreadyExistException)
                    {
                        MessageBox.Show("Ya existe un cliente con el mismo nombre ingresado en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Error inesperado al intentar guardar el cliente, verifique los datos ingresados.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }             
            }
        }

        private void btn_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
