﻿namespace Interfaz
{
    partial class PlanProduccion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgPedidosPlan = new System.Windows.Forms.DataGridView();
            this.btn_FinalizacionPedido = new System.Windows.Forms.Button();
            this.btn_CancelarPlan = new System.Windows.Forms.Button();
            this.btn_GenerarPlan = new System.Windows.Forms.Button();
            this.dgPedidosFueraDeFecha = new System.Windows.Forms.DataGridView();
            this.Llb_Criterio = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgPedidosPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPedidosFueraDeFecha)).BeginInit();
            this.SuspendLayout();
            // 
            // dgPedidosPlan
            // 
            this.dgPedidosPlan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPedidosPlan.Location = new System.Drawing.Point(35, 27);
            this.dgPedidosPlan.Name = "dgPedidosPlan";
            this.dgPedidosPlan.Size = new System.Drawing.Size(544, 236);
            this.dgPedidosPlan.TabIndex = 0;
            // 
            // btn_FinalizacionPedido
            // 
            this.btn_FinalizacionPedido.Location = new System.Drawing.Point(607, 213);
            this.btn_FinalizacionPedido.Name = "btn_FinalizacionPedido";
            this.btn_FinalizacionPedido.Size = new System.Drawing.Size(97, 50);
            this.btn_FinalizacionPedido.TabIndex = 1;
            this.btn_FinalizacionPedido.Text = "Finalización del Pedido";
            this.btn_FinalizacionPedido.UseVisualStyleBackColor = true;
            this.btn_FinalizacionPedido.Click += new System.EventHandler(this.btn_FinalizacionPedido_Click);
            // 
            // btn_CancelarPlan
            // 
            this.btn_CancelarPlan.Location = new System.Drawing.Point(607, 133);
            this.btn_CancelarPlan.Name = "btn_CancelarPlan";
            this.btn_CancelarPlan.Size = new System.Drawing.Size(97, 50);
            this.btn_CancelarPlan.TabIndex = 2;
            this.btn_CancelarPlan.Text = "Cancelación del Plan";
            this.btn_CancelarPlan.UseVisualStyleBackColor = true;
            this.btn_CancelarPlan.Click += new System.EventHandler(this.btn_CancelarPlan_Click);
            // 
            // btn_GenerarPlan
            // 
            this.btn_GenerarPlan.Location = new System.Drawing.Point(607, 77);
            this.btn_GenerarPlan.Name = "btn_GenerarPlan";
            this.btn_GenerarPlan.Size = new System.Drawing.Size(97, 50);
            this.btn_GenerarPlan.TabIndex = 3;
            this.btn_GenerarPlan.Text = "Generación del Plan";
            this.btn_GenerarPlan.UseVisualStyleBackColor = true;
            this.btn_GenerarPlan.Click += new System.EventHandler(this.btn_GenerarPlan_Click);
            // 
            // dgPedidosFueraDeFecha
            // 
            this.dgPedidosFueraDeFecha.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPedidosFueraDeFecha.Location = new System.Drawing.Point(35, 305);
            this.dgPedidosFueraDeFecha.Name = "dgPedidosFueraDeFecha";
            this.dgPedidosFueraDeFecha.Size = new System.Drawing.Size(544, 236);
            this.dgPedidosFueraDeFecha.TabIndex = 4;
            // 
            // Llb_Criterio
            // 
            this.Llb_Criterio.AutoSize = true;
            this.Llb_Criterio.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Llb_Criterio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Llb_Criterio.Location = new System.Drawing.Point(32, 280);
            this.Llb_Criterio.Name = "Llb_Criterio";
            this.Llb_Criterio.Size = new System.Drawing.Size(278, 13);
            this.Llb_Criterio.TabIndex = 5;
            this.Llb_Criterio.Text = "Pedidos que serán entregados fuera de fecha:  ";
            this.Llb_Criterio.Click += new System.EventHandler(this.Llb_Criterio_Click);
            // 
            // PlanProduccion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(729, 561);
            this.Controls.Add(this.Llb_Criterio);
            this.Controls.Add(this.dgPedidosFueraDeFecha);
            this.Controls.Add(this.btn_GenerarPlan);
            this.Controls.Add(this.btn_CancelarPlan);
            this.Controls.Add(this.btn_FinalizacionPedido);
            this.Controls.Add(this.dgPedidosPlan);
            this.Name = "PlanProduccion";
            this.Text = "Plan de Produccion";
            ((System.ComponentModel.ISupportInitialize)(this.dgPedidosPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPedidosFueraDeFecha)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgPedidosPlan;
        private System.Windows.Forms.Button btn_FinalizacionPedido;
        private System.Windows.Forms.Button btn_CancelarPlan;
        private System.Windows.Forms.Button btn_GenerarPlan;
        private System.Windows.Forms.DataGridView dgPedidosFueraDeFecha;
        private System.Windows.Forms.Label Llb_Criterio;
    }
}