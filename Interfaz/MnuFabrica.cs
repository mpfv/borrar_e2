﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dominio;

namespace Interfaz
{
    public partial class MnuFabrica : Form
    {

        // Se crea una unica instancia de la fachada del Dominio
        private Fabrica sistema = Fabrica.Instancia;

        public MnuFabrica()
        {
            InitializeComponent();
            CargaDatosInicial();
        }

        private void ayudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AcercaDe frm = new AcercaDe();
            frm.Show();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListaClientes frm = new ListaClientes();
            frm.Show();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListaProductos frm = new ListaProductos();
            frm.Show();

        }

        private void cargaDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("La carga de datos iniciales se ha realizado correctamente.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.None);
        }

        private void criteriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetCriterio frm = new SetCriterio();
            frm.ShowDialog();
        }

        private void pedidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListaPedidos frm = new ListaPedidos();
            frm.Show();
        }

        private void planesDeProducciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PlanProduccion frm = new PlanProduccion();
            frm.Show();
        }

        private void CargaDatosInicial()
        {
            // Carga de datos de prueba a efectos de facilitar el testing de la aplicacion

            Cliente c1 = new Cliente(), c2 = new Cliente(), c3 = new Cliente();
            Criterio crit = new Criterio(Criterio.tiposCriterio.Entrega, sistema.Pedidos);
            sistema.Criterio = crit;

            try
            {
                this.sistema.AgregarCliente("Cliente 1", 1);
                c1 = this.sistema.Clientes.Last();
            }
            catch (Dominio.Excepciones.AlreadyExistException)
            {
                MessageBox.Show("El cliente ya existe en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            try
            {
                this.sistema.AgregarCliente("Cliente 2", 2);
                c2 = this.sistema.Clientes.Last();
            }
            catch (Dominio.Excepciones.AlreadyExistException)
            {
                MessageBox.Show("El cliente ya existe en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            try
            {
                this.sistema.AgregarCliente("Cliente 3", 3);
                c3 = this.sistema.Clientes.Last();
            }
            catch (Dominio.Excepciones.AlreadyExistException)
            {
                MessageBox.Show("El cliente ya existe en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            Producto p1 = new Producto(), p2 = new Producto(), p3 = new Producto(), p4 = new Producto();

            try
            {
                this.sistema.AgregarProducto("Producto 1", "10","100","1");
                p1 = this.sistema.Productos.Last();
            }
            catch (Dominio.Excepciones.AlreadyExistException)
            {
                MessageBox.Show("El producto ya existe en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            try
            {
                this.sistema.AgregarProducto("Producto 2", "20", "200", "2");
                p2 = this.sistema.Productos.Last();
            }
            catch (Dominio.Excepciones.AlreadyExistException)
            {
                MessageBox.Show("El producto ya existe en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            try
            {
                this.sistema.AgregarProducto("Producto 3", "30", "300", "3");
                p3 = this.sistema.Productos.Last();
            }
            catch (Dominio.Excepciones.AlreadyExistException)
            {
                MessageBox.Show("El producto ya existe en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            try
            {
                this.sistema.AgregarProducto("Producto 4", "40", "400", "4");
                p4 = this.sistema.Productos.Last();
            }
            catch (Dominio.Excepciones.AlreadyExistException)
            {
                MessageBox.Show("El producto ya existe en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Pedido pd1 = new Pedido(), pd2 = new Pedido(), pd3 = new Pedido(), pd4 = new Pedido();
   
            try
            {
                this.sistema.RegistrarPedido(1,DateTime.Now.AddMinutes(60),c1,p1);
                pd1 = this.sistema.Pedidos.Last();
            }
            catch (Dominio.Excepciones.AlreadyExistException)
            {
                MessageBox.Show("El pedido ya existe en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            try
            {
                this.sistema.RegistrarPedido(2, DateTime.Now.AddMinutes(40), c2, p2);
                pd2 = this.sistema.Pedidos.Last();
            }
            catch (Dominio.Excepciones.AlreadyExistException)
            {
                MessageBox.Show("El pedido ya existe en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            try
            {
                this.sistema.RegistrarPedido(3, DateTime.Now.AddMinutes(2), c3, p3);
                pd3 = this.sistema.Pedidos.Last();
            }
            catch (Dominio.Excepciones.AlreadyExistException)
            {
                MessageBox.Show("El pedido ya existe en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            try
            {
                this.sistema.RegistrarPedido(4, DateTime.Now.AddMinutes(8), c1, p1);
                pd4 = this.sistema.Pedidos.Last();
            }
            catch (Dominio.Excepciones.AlreadyExistException)
            {
                MessageBox.Show("El pedido ya existe en el sistema.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void MnuFabrica_Load(object sender, EventArgs e)
        {

        }






    }
}
