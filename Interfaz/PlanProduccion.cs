﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dominio;

namespace Interfaz
{
    public partial class PlanProduccion : Form
    {
        private Fabrica sistema = Fabrica.Instancia;
        private Pedido pedido;

        public PlanProduccion()
        {
            InitializeComponent();
            this.dgPedidosPlan.DataSource = null;
            this.dgPedidosPlan.DataSource = sistema.GetPedidosPlan();
         }

        private void dgPedidos_UpdateDataSource()
        {
            this.dgPedidosPlan.DataSource = null;
            this.dgPedidosPlan.DataSource = sistema.GetPedidosPlan();
        }



        private void btn_FinalizacionPedido_Click(object sender, EventArgs e)
        {
            if (this.dgPedidosPlan.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow row in this.dgPedidosPlan.SelectedRows)
                {
                    pedido = row.DataBoundItem as Pedido;
                    sistema.FinalizarPedido(pedido);
                    sistema.EliminarPedidoPlan(pedido);
                    dgPedidos_UpdateDataSource();
                    
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un pedido a finalizar.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btn_GenerarPlan_Click(object sender, EventArgs e)
        {
           this.dgPedidosFueraDeFecha.DataSource = null;
           dgPedidosFueraDeFecha.DataSource = sistema.GenerarPlan();
           dgPedidos_UpdateDataSource();
            
        }

        private void btn_CancelarPlan_Click(object sender, EventArgs e)
        {
            sistema.CancelarPlan();
            dgPedidos_UpdateDataSource();
            dgPedidosFueraDeFecha.DataSource = null;
            dgPedidosFueraDeFecha.Refresh();
        }

        private void Llb_Criterio_Click(object sender, EventArgs e)
        {

        }


    }
}
