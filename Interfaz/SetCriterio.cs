﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dominio;

namespace Interfaz
{
    public partial class SetCriterio : Form
    {
        private Fabrica sistema = Fabrica.Instancia;

        public SetCriterio()
        {
            InitializeComponent();
            if(sistema.Criterio != null)
                this.Llb_Criterio.Text = "CRITERIO SELECCIONADO: " + this.sistema.Criterio.ToString();
            this.cmb_Criterios.DataSource = System.Enum.GetValues(typeof(Criterio.tiposCriterio));
        }

        private void btn_Cambiar_Click(object sender, EventArgs e)
        {
            
            Criterio c = new Criterio((Criterio.tiposCriterio)cmb_Criterios.SelectedIndex,sistema.Pedidos);
            
            this.sistema.Criterio = c;
            this.Llb_Criterio.Text = "CRITERIO SELECCIONADO: " + this.sistema.Criterio.Tipo.ToString();
            this.Dispose();
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmb_Criterios_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
