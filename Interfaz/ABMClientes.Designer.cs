﻿namespace Interfaz
{
    partial class ABMClientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Lbl_CliNom = new System.Windows.Forms.Label();
            this.lbl_CliPref = new System.Windows.Forms.Label();
            this.txt_CliNom = new System.Windows.Forms.TextBox();
            this.Nrc_CliPref = new System.Windows.Forms.NumericUpDown();
            this.btn_Guardar = new System.Windows.Forms.Button();
            this.btn_Cancelar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Nrc_CliPref)).BeginInit();
            this.SuspendLayout();
            // 
            // Lbl_CliNom
            // 
            this.Lbl_CliNom.AutoSize = true;
            this.Lbl_CliNom.Location = new System.Drawing.Point(25, 36);
            this.Lbl_CliNom.Name = "Lbl_CliNom";
            this.Lbl_CliNom.Size = new System.Drawing.Size(50, 13);
            this.Lbl_CliNom.TabIndex = 0;
            this.Lbl_CliNom.Text = "Nombre: ";
            // 
            // lbl_CliPref
            // 
            this.lbl_CliPref.AutoSize = true;
            this.lbl_CliPref.Location = new System.Drawing.Point(25, 89);
            this.lbl_CliPref.Name = "lbl_CliPref";
            this.lbl_CliPref.Size = new System.Drawing.Size(64, 13);
            this.lbl_CliPref.TabIndex = 1;
            this.lbl_CliPref.Text = "Preferencia:";
            // 
            // txt_CliNom
            // 
            this.txt_CliNom.Location = new System.Drawing.Point(95, 36);
            this.txt_CliNom.Name = "txt_CliNom";
            this.txt_CliNom.Size = new System.Drawing.Size(302, 20);
            this.txt_CliNom.TabIndex = 2;
            // 
            // Nrc_CliPref
            // 
            this.Nrc_CliPref.Location = new System.Drawing.Point(95, 82);
            this.Nrc_CliPref.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.Nrc_CliPref.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.Nrc_CliPref.Name = "Nrc_CliPref";
            this.Nrc_CliPref.Size = new System.Drawing.Size(120, 20);
            this.Nrc_CliPref.TabIndex = 3;
            this.Nrc_CliPref.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btn_Guardar
            // 
            this.btn_Guardar.Location = new System.Drawing.Point(148, 130);
            this.btn_Guardar.Name = "btn_Guardar";
            this.btn_Guardar.Size = new System.Drawing.Size(75, 23);
            this.btn_Guardar.TabIndex = 4;
            this.btn_Guardar.Text = "Guardar";
            this.btn_Guardar.UseVisualStyleBackColor = true;
            this.btn_Guardar.Click += new System.EventHandler(this.btn_Guardar_Click);
            // 
            // btn_Cancelar
            // 
            this.btn_Cancelar.Location = new System.Drawing.Point(229, 130);
            this.btn_Cancelar.Name = "btn_Cancelar";
            this.btn_Cancelar.Size = new System.Drawing.Size(75, 23);
            this.btn_Cancelar.TabIndex = 5;
            this.btn_Cancelar.Text = "Cancelar";
            this.btn_Cancelar.UseVisualStyleBackColor = true;
            this.btn_Cancelar.Click += new System.EventHandler(this.btn_Cancelar_Click);
            // 
            // ABMClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 184);
            this.Controls.Add(this.btn_Cancelar);
            this.Controls.Add(this.btn_Guardar);
            this.Controls.Add(this.Nrc_CliPref);
            this.Controls.Add(this.txt_CliNom);
            this.Controls.Add(this.lbl_CliPref);
            this.Controls.Add(this.Lbl_CliNom);
            this.Name = "ABMClientes";
            this.Text = "Clientes";
            this.Load += new System.EventHandler(this.ABMClientes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Nrc_CliPref)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Lbl_CliNom;
        private System.Windows.Forms.Label lbl_CliPref;
        private System.Windows.Forms.TextBox txt_CliNom;
        private System.Windows.Forms.NumericUpDown Nrc_CliPref;
        private System.Windows.Forms.Button btn_Guardar;
        private System.Windows.Forms.Button btn_Cancelar;
    }
}