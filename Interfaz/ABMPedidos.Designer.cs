﻿namespace Interfaz
{
    partial class ABMPedidos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Cantidad = new System.Windows.Forms.Label();
            this.lbl_FchEntrega = new System.Windows.Forms.Label();
            this.btn_Cancelar = new System.Windows.Forms.Button();
            this.btn_Registrar = new System.Windows.Forms.Button();
            this.Nrc_Cantidad = new System.Windows.Forms.NumericUpDown();
            this.dt_Entrega = new System.Windows.Forms.DateTimePicker();
            this.dgClientes = new System.Windows.Forms.DataGridView();
            this.dgProductos = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.Nrc_Cantidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgClientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgProductos)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_Cantidad
            // 
            this.lbl_Cantidad.AutoSize = true;
            this.lbl_Cantidad.Location = new System.Drawing.Point(117, 254);
            this.lbl_Cantidad.Name = "lbl_Cantidad";
            this.lbl_Cantidad.Size = new System.Drawing.Size(52, 13);
            this.lbl_Cantidad.TabIndex = 0;
            this.lbl_Cantidad.Text = "Cantidad:";
            // 
            // lbl_FchEntrega
            // 
            this.lbl_FchEntrega.AutoSize = true;
            this.lbl_FchEntrega.Location = new System.Drawing.Point(117, 283);
            this.lbl_FchEntrega.Name = "lbl_FchEntrega";
            this.lbl_FchEntrega.Size = new System.Drawing.Size(95, 13);
            this.lbl_FchEntrega.TabIndex = 1;
            this.lbl_FchEntrega.Text = "Fecha de Entrega:";
            // 
            // btn_Cancelar
            // 
            this.btn_Cancelar.Location = new System.Drawing.Point(319, 329);
            this.btn_Cancelar.Name = "btn_Cancelar";
            this.btn_Cancelar.Size = new System.Drawing.Size(75, 23);
            this.btn_Cancelar.TabIndex = 7;
            this.btn_Cancelar.Text = "Cancelar";
            this.btn_Cancelar.UseVisualStyleBackColor = true;
            this.btn_Cancelar.Click += new System.EventHandler(this.btn_Cancelar_Click);
            // 
            // btn_Registrar
            // 
            this.btn_Registrar.Location = new System.Drawing.Point(238, 329);
            this.btn_Registrar.Name = "btn_Registrar";
            this.btn_Registrar.Size = new System.Drawing.Size(75, 23);
            this.btn_Registrar.TabIndex = 6;
            this.btn_Registrar.Text = "Registrar";
            this.btn_Registrar.UseVisualStyleBackColor = true;
            this.btn_Registrar.Click += new System.EventHandler(this.btn_Registrar_Click);
            // 
            // Nrc_Cantidad
            // 
            this.Nrc_Cantidad.Location = new System.Drawing.Point(236, 247);
            this.Nrc_Cantidad.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.Nrc_Cantidad.Name = "Nrc_Cantidad";
            this.Nrc_Cantidad.Size = new System.Drawing.Size(120, 20);
            this.Nrc_Cantidad.TabIndex = 8;
            this.Nrc_Cantidad.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // dt_Entrega
            // 
            this.dt_Entrega.Location = new System.Drawing.Point(236, 277);
            this.dt_Entrega.Name = "dt_Entrega";
            this.dt_Entrega.Size = new System.Drawing.Size(200, 20);
            this.dt_Entrega.TabIndex = 9;
            // 
            // dgClientes
            // 
            this.dgClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgClientes.Location = new System.Drawing.Point(12, 21);
            this.dgClientes.Name = "dgClientes";
            this.dgClientes.Size = new System.Drawing.Size(321, 200);
            this.dgClientes.TabIndex = 10;
            // 
            // dgProductos
            // 
            this.dgProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProductos.Location = new System.Drawing.Point(354, 21);
            this.dgProductos.Name = "dgProductos";
            this.dgProductos.Size = new System.Drawing.Size(321, 200);
            this.dgProductos.TabIndex = 11;
            // 
            // ABMPedidos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 364);
            this.Controls.Add(this.dgProductos);
            this.Controls.Add(this.dgClientes);
            this.Controls.Add(this.dt_Entrega);
            this.Controls.Add(this.Nrc_Cantidad);
            this.Controls.Add(this.btn_Cancelar);
            this.Controls.Add(this.btn_Registrar);
            this.Controls.Add(this.lbl_FchEntrega);
            this.Controls.Add(this.lbl_Cantidad);
            this.Name = "ABMPedidos";
            this.Text = "Administrar Pedidos";
            ((System.ComponentModel.ISupportInitialize)(this.Nrc_Cantidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgClientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgProductos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Cantidad;
        private System.Windows.Forms.Label lbl_FchEntrega;
        private System.Windows.Forms.Button btn_Cancelar;
        private System.Windows.Forms.Button btn_Registrar;
        private System.Windows.Forms.NumericUpDown Nrc_Cantidad;
        private System.Windows.Forms.DateTimePicker dt_Entrega;
        private System.Windows.Forms.DataGridView dgClientes;
        private System.Windows.Forms.DataGridView dgProductos;
    }
}