﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaz.Validaciones
{
    public static class Validaciones
    {
        public static bool IsEmptyField(System.Windows.Forms.Control field)
        {
            bool retorno = false;

            if (retorno = String.IsNullOrEmpty(field.Text))
            {
                field.Focus();
            }

            return retorno;
        }

        public static bool IsDoubleField(System.Windows.Forms.Control field)
        {
            bool retorno = false;

            try
            {
                Double.Parse(field.Text);
                retorno = true;
            }
            catch (Exception)
            {
                field.Focus();
            }

            return retorno;
        }

        public static bool IsIntField(System.Windows.Forms.Control field)
        {
            bool retorno = false;

            try
            {
                int.Parse(field.Text);
                retorno = true;
            }
            catch (Exception)
            {
                field.Focus();
            }

            return retorno;
        }

        public static bool IsDecimalField(System.Windows.Forms.Control field)
        {
            bool retorno = false;

            try
            {
                Decimal.Parse(field.Text);
                retorno = true;
            }
            catch (Exception)
            {
                field.Focus();
            }

            return retorno;
        }

        public static bool IsClientePreferenciaOk(System.Windows.Forms.Control field)
        {
            bool retorno = false;
            Decimal d;

            try
            {
                d = Decimal.Parse(field.Text);
                if (d >= 1 && d <= 10)
                {
                    retorno = true;
                }
            }
            catch (Exception)
            {
                field.Focus();
            }

            return retorno;
        }

        public static bool IsPedidoCantidadOk(System.Windows.Forms.Control field)
        {
            bool retorno = false;
            Decimal d;

            try
            {
                d = Decimal.Parse(field.Text);
                if (d > 0)
                {
                    retorno = true;
                }
            }
            catch (Exception)
            {
                field.Focus();
            }

            return retorno;
        }

        public static bool IsPedidoFechaOk(System.Windows.Forms.Control field)
        {
            bool retorno = false;
            DateTime d;

            try
            {
                d = DateTime.Parse(field.Text);
                if (d >= DateTime.Today)
                {
                    retorno = true;
                }
            }
            catch (Exception)
            {
                field.Focus();
            }

            return retorno;
        }

    }
}
